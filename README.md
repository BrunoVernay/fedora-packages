# fedora-packages

A few Fedora RPM packages I may try to write, `.spec` files and so on.

>This repo is for my personnal research and learning.
If you want to install and use Fedora Packages, chances are that you are looking for: https://copr.fedorainfracloud.org/coprs/ycollet/linuxmao/

I plan to:
- Learn ...
- Keep track of [what I have done (learn)](https://brunovernay.blogspot.com/2018/08/drumgizmo-rpm.html)
- Build MuseScore uptodate version
- Build Guitarix uptodate version
- help debugging?

Fedora did change some of its infrastructure and there are lots of broken links as a result. Even Google does not yelds much results. There are some Search Engine Optimization (SEO) issues. Anyway, here are the links:
- https://apps.fedoraproject.org/packages/
- https://src.fedoraproject.org/
- https://apps.fedoraproject.org/ is huge.

A big dream would be to build [LEV Tools: Purr Data for teaching Electronic Music](https://media.ccc.de/v/lac2018-11-the_levtools_a_modular_toolset_in_purr_data_for_creating_and_teaching_electronic_music) the project looks really excellent!
But .... [PurrData](https://github.com/agraef/purr-data-intro/blob/master/Purr-Data-Intro.md) is *multi-platform* as in "_Windows, Mac, Ubuntu_". BUT [not Fedora](https://puredata.info/docs/developer/Fedora) !? :-)
I started to look at the dependencies. There are a lot! I guess everything might not be needed (Video?) It should be possible to build a light version ... will see.


**Funny note**: Maybe all this is useless, given *AppImage*, *FlatPack*, *Snappy* ... see [MuseScore Download](https://musescore.org/download) for example!
