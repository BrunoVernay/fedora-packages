# MuseScore

MuseScore packaging for Fedora.

- There is already an existing package in the repo.
- `musescore` is an alias to the package name `mscore` (the executable IS `mscore`)


> MuseScore is a sophisticated program made up of millions of lines of code split among thousands of files
https://musescore.org/en/handbook/developers-handbook/compilation/compile-instructions-linux-bsd-git#Tools-for-editing-and-debugging

## Existing resources

### Existing package

At least there is a way to get the source RPM! 

> Be careful to install [fedora-packager](https://docs.fedoraproject.org/en-US/quick-docs/creating-rpm-packages/index.html)  and run `rpmdev-setuptree` prior to the following.

1. `dnf download --source mscore` (as non-root user) will get the file `mscore-2.2.1-1.fc28.src.rpm`
2. `rpm -ivh mscore-2.2.1-1.fc28.src.rpm` will "install" (just decompress) in `~/rpmbuild/`

```
$ rpm -ql mscore-2.2.1-1.fc28.src.rpm
MuseScore-2.0.1-fix-fonts_tabulature.patch
MuseScore-2.2.1.zip
mscore-2.0.3-fix-desktop-file.patch
mscore-2.0.3-separate-commonfiles.patch
mscore-2.2.1-use-default-soundfont.patch
mscore.metainfo.xml
mscore.spec
mscore.xml
musescore-2.0.1-fix-flags-for-precompiled-header.patch

tree ~/rpmbuild
├── SOURCES
│   ├── mscore-2.0.3-fix-desktop-file.patch
│   ├── mscore-2.0.3-separate-commonfiles.patch
│   ├── mscore-2.2.1-use-default-soundfont.patch
│   ├── mscore.metainfo.xml
│   ├── mscore.xml
│   ├── musescore-2.0.1-fix-flags-for-precompiled-header.patch
│   ├── MuseScore-2.0.1-fix-fonts_tabulature.patch
│   ├── MuseScore-2.2.1.zip
├── SPECS
│   └── mscore.spec

```


### On the Web

- Fedora has some serious referencing (SEO) issue, but here are the links
    - https://apps.fedoraproject.org/packages/mscore
    - https://src.fedoraproject.org/rpms/mscore
- Short instructions from the Developer Handbook [Compile instructions (Fedora) - Git](https://musescore.org/en/developers-handbook/compilation/compile-instructions-fedora-git) Just a list of dependencies for Fedora 21, 22.
- [Distribution maintainers](https://musescore.org/en/handbook/developers-handbook/references/distribution-maintainers) Fedora's link is broken and the 2 persons listed show no activities for the last 6 years.
- A thread on MuseScore Forum [Building Musescore 2.0 on Fedora 21](https://musescore.org/en/node/56771) (2015)
  - Seems that MuseScore puts files in unusual locations, requiring packager to write many patches.
  
These are the resource found searching "Fedora" in MuseScore.


## Why?

Searching for MuseScore on [Ask Fedora](https://ask.fedoraproject.org/) yields no answer (except mine). On Fedora Forum, they are dated 2009.

At least this user seems to be using Fedora [Joachim Backes](https://musescore.org/en/user/19120)

## MuseScore build

Just building MuseScore is a challenge, there are many dependencies and modules!
There are lots of reading before understanding: https://github.com/musescore/MuseScore

It uses *CMake* and eventually *QtCreator* to debug.

To check:
- They instruct to `make revision` as a [first step](https://musescore.org/en/handbook/developers-handbook/compilation/compile-instructions-linux-bsd-git#Build-an-executable-file) BUT it is not in the Fedora spec???

By default it install to `/usr/local`. Fedora uses `PREFIX=/usr`.

There is no explanation as to why Fedora spec uses many build options: 
```
DCMAKE_BUILD_TYPE=RELEASE         \
          -DCMAKE_CXX_FLAGS="%{optflags} -fsigned-char"    \
          -DCMAKE_CXX_FLAGS_RELEASE="%{optflags} -std=c++11 -fPIC -O2 -DDEBUG -fsigned-char" \
          -DBUILD_LAME=ON \
...
```

### Patches

There are complaints in the forum that MuseScore puts files in unusual locations and requires packagers to write patches. There is a hard to find page on the subject: [Packaging for Linux & BSD systems (maintainer wishlist)](https://musescore.org/en/developers-handbook/packaging-different-operating-systems/packaging-linux-bsd-systems-maintainer-wis)

Indeed the Fedora package contains 5 patches (some may not be required anymore.)

- `mscore-2.3.2-explicit-qt-version.patch` 
- `mscore-2.3.2-use-default-soundfont.patch`
- `mscore-2.0.3-fix-desktop-file.patch` The `Version` is exactly what can be found in the FreeDesktop example: https://standards.freedesktop.org/desktop-entry-spec/latest/apa.html This patch seems useless.
- `mscore-2.0.3-separate-commonfiles.patch` deals with the `:/data` Qt things that I detected in the test. TODO re-integrate ... 
- `mscore-2.2.1-use-default-soundfont.patch` Replaced by new version 
- `mscore.metainfo.xml` 
- `mscore.xml`
- `musescore-2.0.1-fix-flags-for-precompiled-header.patch`

#### Uses /user/share instead of Qt internal path

It is about the `:/data` and `:/fonts` path pointing to internal Qt resources.

It is fine for the icons and the "internal" stuff, mostly what is found in the `*.ui` files.  But it is not very natural, nor handy, to find these path in the interface, exposed to the user.

- There was a patch on the subject: `mscore-2.0.3-separate-commonfiles.patch`.
- Also it is deal with for the SoundFonts.

To scan all the places where it is used: `ag \":/ --ignore \*.ui`

I will mostly update the patch.
- `effects/zita1/zitagui.cpp` has some png, I guess can be kept as resource
- `libmscore/chordlist.cpp` no patch. the `:/` is for Android 
- `libmscore/figuredbass.cpp` patch done
- `libmscore/mscore.cpp` patch done
- `libmscore/stafftype.cpp` patch done
- `libmscore/sym.cpp` patch done
- `mscore/editstafftype.cpp` ??
- `mscore/file.cpp` ??
- `mscore/importxml.cpp` keep as resource
- `mscore/newwizard.cpp` keep as resource
- `mscore/musescore.cpp` eaxamine TODO /data/My_First_Score.mscz
- `mscore/preferences.cpp` patch done
- `mscore/startcenter.cpp` ?? 
- `mscore/shortcut.cpp` ??
- `mstyle/mstyle.cpp` keep as resource

I tried to follow the install PREFIX and not hardcode the path !!!

Note that removing some lines from the `mscore/musescore.qrc`, does reduce the package size. I guess files does not get stored internally. TODO double check!

Must be cautious to TEST TODO all these changes:
- They might be useless, often the `/usr/share` path is proposed in the "Open file" dialog, in the "Look in" part.
- They might break stuff.

### First build

First I try to build on the machine directly, without going through the RPM Source.

There are important files:
- `CMakeLists.txt` contains many options and hints about what is possible.
  - TODO: OMR: Convert PDF to MIDI !!! (with OCR)
  - `BUILD_PORTMIDI` should be disabled on Linux, but is commented!?
  - `USE_SYSTEM_QTSINGLEAPPLICATION` already leverage in the spec file.
  - I apply a patch equivalent to this commit: https://github.com/musescore/MuseScore/pull/3854

Initialization (run just once!):
```
mkdir -p ~/mstest
rm -rf MuseScore-2.3.2
tar xf MuseScore-2.3.2.tar.gz
cd MuseScore-2.3.2 
patch -p0 -i ~/rpmbuild/SOURCES/mscore-2.3.2-use-default-soundfont.patch 
patch -p0 -i ~/rpmbuild/SOURCES/mscore-2.3.2-explicit-qt-version.patch 
sed -i 's|-lporttime||' mscore/CMakeLists.txt
```
Then
```
take build.release
cmake -DCMAKE_BUILD_TYPE=RELEASE \
  -DCMAKE_INSTALL_PREFIX=$HOME/mstest  \
  -DMUSESCORE_LABEL="bve-2018" \
  -DMSCORE_INSTALL_SUFFIX="-bve" \
  -DUSE_SYSTEM_QTSINGLEAPPLICATION=YES \
  -DUSE_PATH_WITH_EXPLICIT_QT_VERSION=YES  \
  -DBUILD_PORTAUDIO=OFF -DBUILD_PORTMIDI=OFF \
  -DUSE_SYSTEM_FREETYPE=YES \
  -DCMAKE_SKIP_RPATH=ON ..

make lrelease
make -j8
make install
```

and I can launch `~/mstest/bin/mscore` and it works!!!!


TODO is it necessary to delete the unwanted module, are they build anyway?? 
  It build `bww`, `awl`, `bww2msxml` ... that are not required, nor wanted.
  Trying to delete `bww2msxml` directory AND include in '`CMakeList.txt` yelds to an compile error later on ...

#### make vs. cmake

I finally understood that there are 2 ways to build:
- `make` targets `revision` will launche  cmake automatically
- `cmake` targets `lrelease`, `all`, *MUST* be launched from a sub-directory, or it will overwrite the existing `Makefile`!!! Then, no more `release`, `uninstall` ... 

I used to launch `cmake` in the source directory! That is why:
- `revision` and `release` are not known targets 
- `PREFIX` does not work. Using `CMAKE_INSTALL_PREFIX` instead DOES work.
- `make clean` does not seem to be enough. I remove the whole directory and start again from the tarball.
- No `make uninstall` 
- Had to use `lrelease`, as in the spec file, "l" is for languages.
- Then `make` will make `all` . (better not forget the `-j8` option ...)

Reading the original `Makefile` a fixed set of variable `-D...` can be transmitted to `cmake`, but we cannot pass random variables.  Hence I have to use the cmake first style.

TODO it should be possible to get the uninstall target reading this original `Makefile`!
TODO there could be a warning if `cmake` is launched from within the source directory.

#### Testing
  
Cannot resist to test this early version 2.3.2!!
Biggest bug so far: the _Lute 9-course_ is mapped on a _Steel String Guitar_, go figure :-)

The interface looks slightly better, I guess from Qt upgrade.

The Plugins / Plugin Manager seems new, Coloring notes is fun.
The Help / Resource manager seems new too. Been able to install the Musescore Drumline, MDL looks great! (but nobody will find it in the Help menu!). Uninstalling produces a Core dump ...

Still no MIDI ouput per instrument channel ??
- https://musescore.org/en/node/110931
- See also https://musescore.org/en/node/208986

When Jack audio is selected, it outputs tons of logs in the console!
It has to be selected for MIDI to work in and out.

## RPM tests and Build

1. Download the sources: `spectool -D -g SPECS/mscore.spec -R`
2. Build the Source RPM: `rpmbuild -bs SPECS/mscore.spec`
3. Test the dependencies: `mock SRPMS/mscore-2.3.2-1.fc28.src.rpm` (more than 500 packages, 1Go better read how to optimize and setup a cache.)

You can loop the 2 and 3 steps above, eventually run mock with `--no-clean` to speed up things.

4. Build all the RPM: `rpmbuild -ba SPECS/mscore.spec`. It will produce 3 packages!
     - `RPMS/noarch/mscore-doc-2.3.2...` as if you want to save 41ko ...
     - `RPMS/noarch/mscore-fonts-2.3.2...` 4.2Mo, what is the point?
     - `RPMS/x86_64/mscore-2.3.2...` 54Mo, the real stuff.

5. Install `sudo dnf install ~/rpmbuild/RPMS/x86_64/mscore-2.3.* ~/rpmbuild/RPMS/noarch/mscore-{doc,fonts}-*`
6. Clean-up config `rm -rf  ~/.config/MuseScore ~/.local/share/data/MuseScore`


### Tests

Currently, it works. But,
- in the Preference / Score, Instrument list, there is  `:/data/instruments.xml`
- in the Preference / Canvas, Wallpaper I have a `:/data/...` path too ??
- In Gnome:
     - MuseScore does not seem to be installed at first, one has to log off then in and it is OK.
     - The desciption could be better: Doc, Screenshot ...
     - MuseJazz (the Font) appears but why? Why not the others?

TODO: FIX !!!

The `:/data` is "normal" for what I read in MuseScore forums and is a Qt things: http://doc.qt.io/qt-5/resources.html . I patched `mscore/preferences.cpp` still, because it would feel more natural to have a real existing path! There was a patch for it.


The configuration is in 2 places. https://musescore.org/en/handbook/revert-factory-settings : 
- ` ~/.config/MuseScore/MuseScore2.ini`
- `~/.local/share/data/MuseScore/MuseScore2/`
- `rm -rf  ~/.config/MuseScore ~/.local/share/data/MuseScore`

### Changes

Simplifications

`%setup %prep %patch` uses minimal option. `-q` for Quiet. And the name because the tarball `v-2.3.2` expand to `MuseScore...`


Removed

```
# Force Fedora specific flags:
find . -name CMakeLists.txt -exec sed -i -e 's|-m32|%{optflags}|' -e 's|-O3|%{optflags}|' {} \;
```
I searched for -m32 or -03 in all sources. They are nowhere to be found.


```
# Do not build the bundled qt scripting interface:
sed -i 's|BUILD_SCRIPTGEN TRUE|BUILD_SCRIPTGEN FALSE|' %{name}/CMakeLists.txt
```
Same: `BUILD_SCRIPTGEN` is not in the file ...


```
#mkdir -p %{buildroot}/%{_datadir}/mime/packages
#install -pm 644 %{SOURCE1} %{buildroot}/%{_datadir}/mime/packages/
```
MScore already builds a `share/mime/packages/musescore.xml` but I am not so sure about installation.



#### 

#### Removed Fluid-SoundFonts obsolete dependency

There are 2 dependencies: 
- `fluid-soundfont-common` 87ko. README, PDF and License dated 2002 (almost 20 years!) All the links are dead, even the domains does not exist.
- `fluid-soundfont-gm` 120Mo Contains outdated sf2 Soundfonts, 


## Packaging is dead

I did not expect such a complexity to build a music notation software!

- License complexities:
      - Some content has to be pulled off
      - All the different licenses have to be gathered, checked, displayed
- Distribution organization:
      - files have to be moved to the "right" distribution's directories (which is slightly different for each distrib)
      - Code has to be patched to use the "right" distribution's includes and files organization
      - Mime types, desktop files, Gnome metainfo ...
- Packaging policy? The most gratuitous complexity
      - Why does it have to be split in 3 packages?  Who wants to deal with the doc and the fonts separately? 
      - You remove mscore, but you still have mscore-fonts ...
      - I found that there are even dependencies: `fluid-soundfont-common` and `fluid-soundfont-gm`, will have to check out what they are ....

