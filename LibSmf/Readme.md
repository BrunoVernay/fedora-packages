# LibSmf 

Spec file to build [LibSmf library](https://github.com/stump/libsmf). 

> C library for handling Standard MIDI Files (*.mid)

It looks like a one person project, but quite stable. Handling MIDI files seems like a rather common task, not sure why it is not popular.

The releases are not so frequent and the code stable (I guess) hence we are building a commit, not a release. (Again, I follow [Libsmf build already done](https://github.com/ycollet/fedora-spec/tree/master/drumgizmo))

## Why?

It is required for DrumGizmo Command Line tool. (but you can build the LV2 plugin without it.)

It is a simple library with few dependencies, great for learning.

Note: Ardour has the library *bundled*, *embedded* or "Hard-linked". Not sure if there is a risk using a plugin that would have another version of the library ???

## How-To test and build

Mock ...

TODO ...

Maybe to most complicated part is that we use a specific commit instead of a release.


