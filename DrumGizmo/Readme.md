# DrumGizmo

In itself, it does not do much: No sequencer, no sounds ... But it will load a sound bank (up to a few Go ...) and play them with velocity and timing humanization, distribute sounds over multiple channels ... 

Also it uses its own proprietary SoundFont format. No `sf2`, `sf3` or `sfz` and no way to use [the 45 freely available unpitched percussions Soundfonts](https://www.polyphone-soundfonts.com/en/soundfonts/percussion/unpitched-percussion) you get only about 5. There is a SoundFont editor in early beta: [DGEdit](https://drumgizmo.org/wiki/doku.php?id=dgedit:roadmap) 

It comes as 2 shapes:

- LV2 plugin instrument.
- Command Line tool, either to play a MIDI file or to e used as a *Jack MIDI* client.


## Why

Opportunity to build a recent 0.9.15 version.

Not to complicated, but not trivial either (dependency, options, QA). Great intermediate learning experience.


## How To test and Build

TODO: copy https://brunovernay.blogspot.com/2018/08/drumgizmo-rpm.html 


### Dependency

`LibSmf-devel` See my other project.



